//
//  Solver.cpp
//  PhiloSAT
//
//  Created by Philipp Friese on 25.11.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#include "Solver.hpp"

int SET_CONFLICT = -1;
int SET_SUCCESS = 0;

int UNIT_CONFLICT = -1;
int UNIT_SUCCESS = 0;

int PURE_CONFLICT = -1;
int PURE_SUCCESS = 0;

int BACKTRACK_UNSAT = -1;
int BACKTRACK_SUCCESS = 0;

unsigned long int MOM_EXPONENT = static_cast<unsigned long int>(pow(2,5));

literal Solver::jw()
{
    double maxOccurences = 0.0;
    literal maxLiteral = {};
    for(const auto &v : formula->variables) {
        if(v->value != vFree)
            continue;
        double sum = 0;
        for(const auto &c : v->positiveOccurences) {
            if(c->sat)
                continue;
            double temp = 1.0 / static_cast<double>(c->literals.size());
            sum += temp;
        }
        for(const auto &c : v->negativeOccurences) {
            if(c->sat)
                continue;
            double temp = 1.0 / pow(2, static_cast<double>(c->literals.size()));
            sum += temp;
        }
        
        if(sum > maxOccurences) {
            maxOccurences = sum;
            maxLiteral = {v->id, vTrue};
        }
    }
    
    return maxLiteral;
}

literal Solver::mom()
{
    int minClauseLength = formula->numVariables;
    for(const auto &c : formula->clauses) {
        if(!c->sat && c->activeLiterals < minClauseLength) {
            minClauseLength = c->activeLiterals;
        }
    }
    
    unsigned long maxOccurences = 0;
    literal maxLiteral = {};
    for(const auto &c : formula->clauses) {
        if(!c->sat && c->activeLiterals == minClauseLength) {
            for(const auto &l : c->literals) {
                auto v = formula->variableFor(*l);
                if(v->value != vFree)
                    continue;
                auto momSum = (v->positiveOccurences.size() + v->negativeOccurences.size()) * MOM_EXPONENT + (v->positiveOccurences.size() * v->negativeOccurences.size());
                if(momSum > maxOccurences) {
                    maxOccurences = momSum;
                    maxLiteral = {l->id, ((v->positiveOccurences.size() >= v->negativeOccurences.size()))};
                }
            }
        }
    }
    return maxLiteral;
}

literal Solver::dlcs()
{
    int sum = 0;
    literal l = {};
    for(const auto &v : formula->variables) {
        auto vsum = static_cast<int>(v->positiveOccurences.size() + v->negativeOccurences.size());
        if(v->value == vFree && vsum > sum) {
            sum = vsum;
            l = {v->id, ((v->positiveOccurences.size() >= v->negativeOccurences.size()))};
        }
    }
    return l;
}

literal Solver::dlis()
{
    int sum = 0;
    literal l = {};
    for(const auto &v : formula->variables) {
        int vsum = 0;
        if(v->value != vFree)
            continue;
        for(const auto &c : v->positiveOccurences) {
            if(c->sat)
                continue;
            vsum++;
        }
        if(vsum > sum) {
            sum = vsum;
            l = {v->id, true};
        }
    }
    return l;
}

literal Solver::naive()
{
    for(const auto &v : formula->variables)
        if(v->value == vFree) {
            return {v->id, true};
        }
    return {};
}

literal Solver::pick()
{
    switch (ptype) {
        case pickType::naive: return naive();
        case pickType::dlis:  return dlis();
        case pickType::dlcs:  return dlcs();
        case pickType::mom:   return mom();
        case pickType::jw:    return jw();
    }
    return {};
}

int Solver::set(literal l, vValue value, assignmentType type)
{
    int status = SET_SUCCESS;
    auto currentVariable = formula->variableFor(l);
    auto oldValue = currentVariable->value;
    
    assignment assign = {type, l, oldValue, value};
    alpha.push_back(assign);

    currentVariable->value = value;
    
    std::vector<std::shared_ptr<clause>> *po = nullptr;
    std::vector<std::shared_ptr<clause>> *no = nullptr;
    
    if(value == vTrue) {
        po = &(currentVariable->positiveOccurences);
        no = &(currentVariable->negativeOccurences);
    } else {
        po = &(currentVariable->negativeOccurences);
        no = &(currentVariable->positiveOccurences);
    }
    
    for(const auto &c : *po) {
        if (!c->sat) {
            c->sat = true;
            c->satBy = l.id;
        }
    }
    for(const auto &c : *no) {
        if(!c->sat) {
            --(c->activeLiterals);
            
            if(c->activeLiterals == 1) {
                for(auto lit : c->literals) {
                    if(formula->variableFor(*lit)->value == vFree) {
                        if(std::find_if(units.begin(), units.end(), [&lit](const auto& x) { return x.id == lit->id;}) == units.end())
                            units.push_back(*lit);
                    }
                }
            }
            else if(c->activeLiterals == 0)
                status = SET_CONFLICT;
        }
    }
    return status;
}

void Solver::unset(literal l)
{
    auto currentVariable = formula->variableFor(l);
    vValue oldValue = currentVariable->value;
    if(oldValue == vFree)
        return;
    
    std::vector<std::shared_ptr<clause>> *po = nullptr;
    std::vector<std::shared_ptr<clause>> *no = nullptr;
    
    if(oldValue == vTrue) {
        po = &(currentVariable->positiveOccurences);
        no = &(currentVariable->negativeOccurences);
    } else if (oldValue == vFalse){
        po = &(currentVariable->negativeOccurences);
        no = &(currentVariable->positiveOccurences);
    }
    else
        std::cout << "Invalid oldValue on unset!" << std::endl;

    currentVariable->value = vFree;
    
    if(po != nullptr)
        for(const auto &c : *po) {
            if(c->sat && c->satBy == l.id) {
                c->sat = false;
                c->satBy = -1;
            }
        }
    if(no != nullptr)
        for(const auto &c : *no)
            if(!c->sat)
                ++(c->activeLiterals);
}

int Solver::backtrack()
{
    bool doBacktrack = true;
    while(doBacktrack) {
        auto bloop = static_cast<assignment>(alpha.back());
        if(bloop.type == forced) {
            alpha.pop_back();
            unset(bloop.l);
        } else
            doBacktrack = false;
        if(alpha.empty())
            return BACKTRACK_UNSAT;
    }
    
    if(alpha.empty())
        return BACKTRACK_UNSAT;
    
    auto b = static_cast<assignment>(alpha.back());
    alpha.pop_back();
    unset(b.l);
    
    units.clear();
    
    set(b.l, (b.newValue == vTrue) ? vFalse : vTrue, forced);

    return BACKTRACK_SUCCESS;
}


int Solver::pureLiteralElimination()
{
    for(const auto &v : formula->variables) {
        if(v->value != vFree)
            continue;
        
        int sumPos = 0, sumNeg = 0;
        std::for_each(v->positiveOccurences.begin(), v->positiveOccurences.end(),[&sumPos](auto &c) { if(!c->sat) ++sumPos; });
        std::for_each(v->negativeOccurences.begin(), v->negativeOccurences.end(),[&sumNeg](auto &c) { if(!c->sat) ++sumNeg; });
        
        if((sumPos == 0) ^ (sumNeg == 0))
            pures.push_back({v->id, sumPos != 0});
    }
    
    while(!pures.empty()) {
        literal b = pures.back();
        pures.pop_back();
        
        auto res = set(b, (b.sign ? vTrue : vFalse), forced);
        if(res == SET_CONFLICT)
            return PURE_CONFLICT;
    }
    return PURE_SUCCESS;
}

int Solver::unitPropagation()
{
    while(!units.empty()) {
        if(m_timeout)
            return UNIT_CONFLICT;
        
        literal b = units.back();
        units.pop_back();
        
        auto res = set(b, (b.sign ? vTrue : vFalse), forced);
        if(res == SET_CONFLICT)
            return UNIT_CONFLICT;
    }
    
    return UNIT_SUCCESS;
}


std::vector<assignment> Solver::solve()
{
    for(const auto &c : formula->clauses)
        if(c->activeLiterals == 1)
            units.push_back(*(c->literals[0]));
    
    pureLiteralElimination();
    unitPropagation();
    while(true) {
        if(m_timeout)
            return {};
        literal l = pick();
        
        set(l, l.sign ? vTrue : vFalse, branching);
        
        auto ures = unitPropagation();
        auto pres = pureLiteralElimination();
        
        while(ures == UNIT_CONFLICT || pres == PURE_CONFLICT) {
            ures = UNIT_SUCCESS;
            pres = PURE_SUCCESS;
            auto bres = backtrack();
            
            if(bres == BACKTRACK_UNSAT)
                return {};
            if(!units.empty())
                ures = unitPropagation();
            if(!pures.empty())
                pres = pureLiteralElimination();
        }
        
        
        bool sat = true;
        for(const auto &c : formula->clauses) {
            sat &= c->sat;
            if(!sat)
                break;
        }
        
        if(sat)
            return alpha;
    }
}

