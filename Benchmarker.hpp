//
//  Benchmarker.hpp
//  PhiloSAT
//
//  Created by Philipp Friese on 15.12.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#ifndef Benchmarker_hpp
#define Benchmarker_hpp

#include "Solver.hpp"
#include "Formula.hpp"

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <chrono>
#include <future>
#include <thread>
#include <mutex>
#include <map>

class Benchmarker {
public:
    std::chrono::nanoseconds benchmark(std::string filename, pickType ptype, bool *sat = nullptr, bool *timeout = nullptr);
    void benchmarkAll();
private:
    
};

#endif /* Benchmarker_hpp */
