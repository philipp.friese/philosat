//
//  Benchmarker.cpp
//  PhiloSAT
//
//  Created by Philipp Friese on 15.12.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#include "Benchmarker.hpp"

const std::vector<std::tuple<std::string, std::vector<std::string>>> groups {
    {"aim", {"aim-100-1_6-no-1.cnf",
        "aim-100-1_6-no-2.cnf",
        "aim-100-1_6-no-3.cnf",
        "aim-100-1_6-no-4.cnf",
        "aim-100-1_6-yes1-1.cnf",
        "aim-100-1_6-yes1-2.cnf",
        "aim-100-1_6-yes1-3.cnf",
        "aim-100-1_6-yes1-4.cnf",
        "aim-100-2_0-no-1.cnf",
        "aim-100-2_0-no-2.cnf",
        "aim-100-2_0-no-3.cnf",
        "aim-100-2_0-no-4.cnf",
        "aim-100-2_0-yes1-1.cnf",
        "aim-100-2_0-yes1-2.cnf",
        "aim-100-2_0-yes1-3.cnf",
        "aim-100-2_0-yes1-4.cnf",
        "aim-100-3_4-yes1-1.cnf",
        "aim-100-3_4-yes1-2.cnf",
        "aim-100-3_4-yes1-3.cnf",
        "aim-100-3_4-yes1-4.cnf",
        "aim-100-6_0-yes1-1.cnf",
        "aim-100-6_0-yes1-2.cnf",
        "aim-100-6_0-yes1-3.cnf",
        "aim-100-6_0-yes1-4.cnf",
        "aim-200-1_6-no-1.cnf",
        "aim-200-1_6-no-2.cnf",
        "aim-200-1_6-no-3.cnf",
        "aim-200-1_6-no-4.cnf",
        "aim-200-1_6-yes1-1.cnf",
        "aim-200-1_6-yes1-2.cnf",
        "aim-200-1_6-yes1-3.cnf",
        "aim-200-1_6-yes1-4.cnf",
        "aim-200-2_0-no-1.cnf",
        "aim-200-2_0-no-2.cnf",
        "aim-200-2_0-no-3.cnf",
        "aim-200-2_0-no-4.cnf",
        "aim-200-2_0-yes1-1.cnf",
        "aim-200-2_0-yes1-2.cnf",
        "aim-200-2_0-yes1-3.cnf",
        "aim-200-2_0-yes1-4.cnf",
        "aim-200-3_4-yes1-1.cnf",
        "aim-200-3_4-yes1-2.cnf",
        "aim-200-3_4-yes1-3.cnf",
        "aim-200-3_4-yes1-4.cnf",
        "aim-200-6_0-yes1-1.cnf",
        "aim-200-6_0-yes1-2.cnf",
        "aim-200-6_0-yes1-3.cnf",
        "aim-200-6_0-yes1-4.cnf",
        "aim-50-1_6-no-1.cnf",
        "aim-50-1_6-no-2.cnf",
        "aim-50-1_6-no-3.cnf",
        "aim-50-1_6-no-4.cnf",
        "aim-50-1_6-yes1-1.cnf",
        "aim-50-1_6-yes1-2.cnf",
        "aim-50-1_6-yes1-3.cnf",
        "aim-50-1_6-yes1-4.cnf",
        "aim-50-2_0-no-1.cnf",
        "aim-50-2_0-no-2.cnf",
        "aim-50-2_0-no-3.cnf",
        "aim-50-2_0-no-4.cnf",
        "aim-50-2_0-yes1-1.cnf",
        "aim-50-2_0-yes1-2.cnf",
        "aim-50-2_0-yes1-3.cnf",
        "aim-50-2_0-yes1-4.cnf",
        "aim-50-3_4-yes1-1.cnf",
        "aim-50-3_4-yes1-2.cnf",
        "aim-50-3_4-yes1-3.cnf",
        "aim-50-3_4-yes1-4.cnf",
        "aim-50-6_0-yes1-1.cnf",
        "aim-50-6_0-yes1-2.cnf",
        "aim-50-6_0-yes1-3.cnf",
        "aim-50-6_0-yes1-4.cnf"}},
    {"uf", {"uf50-01.cnf",
        "uf50-010.cnf",
        "uf50-011.cnf",
        "uf50-012.cnf",
        "uf50-013.cnf",
        "uf50-014.cnf",
        "uf50-015.cnf",
        "uf50-016.cnf",
        "uf50-017.cnf",
        "uf50-018.cnf",
        "uf50-019.cnf",
        "uf50-02.cnf",
        "uf50-03.cnf",
        "uf50-04.cnf",
        "uf50-05.cnf",
        "uf50-06.cnf",
        "uf50-07.cnf",
        "uf50-08.cnf",
        "uf50-09.cnf"}},
    {"ssa", {"ssa0432-003.cnf",
        "ssa2670-130.cnf",
        "ssa2670-141.cnf",
        "ssa6288-047.cnf",
        "ssa7552-038.cnf",
        "ssa7552-158.cnf",
        "ssa7552-159.cnf",
        "ssa7552-160.cnf"}},
    {"other", {
        "rand3.cnf",
        "rand6.cnf",
        "count6_2.cnf",
        "unique.cnf"
    }},
    {"pret", {"pret150_25.cnf",
        "pret150_40.cnf",
        "pret150_60.cnf",
        "pret150_75.cnf",
        "pret60_25.cnf",
        "pret60_40.cnf",
        "pret60_60.cnf",
        "pret60_75.cnf"}},
    {"par", {"par16-1-c.cnf",
        "par16-1.cnf",
        "par16-2-c.cnf",
        "par16-2.cnf",
        "par16-3-c.cnf",
        "par16-3.cnf",
        "par16-4-c.cnf",
        "par16-4.cnf",
        "par16-5-c.cnf",
        "par16-5.cnf",
        "par8-1-c.cnf",
        "par8-1.cnf",
        "par8-2-c.cnf",
        "par8-2.cnf",
        "par8-3-c.cnf",
        "par8-3.cnf",
        "par8-4-c.cnf",
        "par8-4.cnf",
        "par8-5-c.cnf",
        "par8-5.cnf"}},
    {"ii", {"ii16a1.cnf",
        "ii16a2.cnf",
        "ii16b1.cnf",
        "ii16b2.cnf",
        "ii16c1.cnf",
        "ii16c2.cnf",
        "ii16d1.cnf",
        "ii16d2.cnf",
        "ii16e1.cnf",
        "ii16e2.cnf",
        "ii32a1.cnf",
        "ii32b1.cnf",
        "ii32b2.cnf",
        "ii32b3.cnf",
        "ii32b4.cnf",
        "ii32c1.cnf",
        "ii32c2.cnf",
        "ii32c3.cnf",
        "ii32c4.cnf",
        "ii32d1.cnf",
        "ii32d2.cnf",
        "ii32d3.cnf",
        "ii32e1.cnf",
        "ii32e2.cnf",
        "ii32e3.cnf",
        "ii32e4.cnf",
        "ii32e5.cnf",
        "ii8a1.cnf",
        "ii8a2.cnf",
        "ii8a3.cnf",
        "ii8a4.cnf",
        "ii8b1.cnf",
        "ii8b2.cnf",
        "ii8b3.cnf",
        "ii8b4.cnf",
        "ii8c1.cnf",
        "ii8c2.cnf",
        "ii8d1.cnf",
        "ii8d2.cnf",
        "ii8e1.cnf",
        "ii8e2.cnf"}},
    {"hole", {"hole10.cnf",
        "hole2.cnf",
        "hole5.cnf",
        "hole6.cnf",
        "hole7.cnf",
        "hole8.cnf",
        "hole9.cnf"}}
};

std::vector<int> solveFormulaBenchmarked(Solver &s)
{
    auto result = s.solve();
    if(result.empty())
        return {};

    std::vector<literal> satAssignment;

    while (!result.empty()) {
        assignment alpha = result.back();
        satAssignment.push_back({alpha.l.id, alpha.newValue == vTrue});
        result.pop_back();
    }

    sort(satAssignment.begin(), satAssignment.end(), []( const auto& lhs, const auto& rhs)
         {
             return abs(lhs.id) < abs(rhs.id);
         });

    std::stringstream ss;
    std::vector<int> solution;
    solution.reserve(satAssignment.size());
    for(auto &element : satAssignment)
        solution.push_back((element.sign ? 1 : -1) * element.id);

    return solution;
}


void Benchmarker::benchmarkAll()
{
    std::mutex m;
    
    std::thread t1, t2, t3, t4, t5;
    for(auto group : groups) {
        auto groupName = std::get<0>(group);
        auto filenames = std::get<1>(group);
        
        std::stringstream ss;
        ss << "./benchmark_" << groupName << ".csv";
        
        std::ofstream benchmarkResults (ss.str());
        if(!benchmarkResults.is_open()) {
            std::cerr << "Benchmark Result File could not be opened!" << std::endl;
            return;
        }
        benchmarkResults << "Filename;naive;dlis;dlcs;mom;jw\n";
        for(auto fn : filenames) {
            std::cout << "Benchmarking " << fn << std::endl;
            std::map<pickType, long int> benchmarks;

            auto threadedBenchmark = [&](pickType ptype) {
                m.lock();
                Formula f(fn);
                m.unlock();
                auto time = benchmark(f, ptype);

                m.lock();
                benchmarks[ptype] = time.count();
                m.unlock();
            };

            t1 = std::thread(threadedBenchmark, pickType::naive);
            t2 = std::thread(threadedBenchmark, pickType::dlis);
            t3 = std::thread(threadedBenchmark, pickType::dlcs);
            t4 = std::thread(threadedBenchmark, pickType::mom);
            t5 = std::thread(threadedBenchmark, pickType::jw);

            t5.join();
            t4.join();
            t3.join();
            t2.join();
            t1.join();

            benchmarkResults << fn;
            for(auto key : {pickType::naive, pickType::dlis, pickType::dlcs, pickType::mom, pickType::jw})
                benchmarkResults << ";" << benchmarks[key];
            benchmarkResults << "\n";
            
            benchmarkResults.close();
        }
    }
}

std::chrono::nanoseconds Benchmarker::benchmark(Formula &formula, pickType ptype)
{
    Solver s(formula);
    s.ptype = ptype;
    std::future<std::vector<int>> future = std::async(std::launch::async, [&](){
        return solveFormulaBenchmarked(s);
    });

    std::future_status status;

    auto start = std::chrono::high_resolution_clock::now();
    status = future.wait_for(std::chrono::seconds(15));
    auto end = std::chrono::high_resolution_clock::now();

    if(status == std::future_status::timeout) {
        s.m_timeout = true;
        return std::chrono::milliseconds(0);
    } else {
        std::vector<int> philosat(future.get());
    }

    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    return duration;
}

