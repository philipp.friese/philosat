//
//  Formula.hpp
//  PhiloSAT
//
//  Created by Philipp Friese on 25.11.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#ifndef Formula_hpp
#define Formula_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <sstream>
#include <memory>
#include <algorithm>
#include <optional>
#include <iterator>

class literal {
public:
    int id;
    bool sign;
    
    bool operator==(const literal &other) { return (id == other.id && sign == other.sign); }
};


class clause {
public:
    std::vector<std::shared_ptr<literal>> literals;
    
    std::shared_ptr<literal> watched1;
    std::shared_ptr<literal> watched2;
};

class variable{
public:
    int id;
    std::optional<bool> value;
    std::vector<std::tuple<std::shared_ptr<clause>, bool>> watchList;
    double priority;
    int activity;
};

class Formula {
public:
    explicit Formula(std::string _filename);
    
    std::string filename;
    
    int numVariables;
    int numClauses;
    
    std::vector<std::shared_ptr<variable>> variables;
    std::vector<std::shared_ptr<clause>> clauses;
    
    inline std::shared_ptr<variable> variableFor(literal &l) { return this->variables.at(static_cast<unsigned long>(l.id-1));};
};

#endif /* Formula_hpp */
