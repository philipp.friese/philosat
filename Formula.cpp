//
//  Formula.cpp
//  PhiloSAT
//
//  Created by Philipp Friese on 25.11.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#include "Formula.hpp"

std::string & ltrim(std::string & str)
{
    auto it2 =  std::find_if( str.begin() , str.end() , [](char ch){ return !std::isspace<char>(ch , std::locale::classic() ) ; } );
    str.erase( str.begin() , it2);
    return str;
}

std::string & rtrim(std::string & str)
{
    auto it1 =  std::find_if( str.rbegin() , str.rend() , [](char ch){ return !std::isspace<char>(ch , std::locale::classic() ) ; } );
    str.erase( it1.base() , str.end() );
    return str;
}

void trim(std::string &str) {
    ltrim(str);
    rtrim(str);
}

void split(const std::string& str, std::vector<std::string>& cont)
{
    std::istringstream iss(str);
    std::copy(std::istream_iterator<std::string>(iss), std::istream_iterator<std::string>(), std::back_inserter(cont));
}


Formula::Formula(std::string _filename) : filename(_filename)
{
    std::ifstream file(filename);
    if(!file.is_open()) {
        std::cerr << "Error: " << filename << " could not be opened" << std::endl;
        exit(-1);
    }
    
    std::string line;
    std::vector<literal> literals;
    while (std::getline(file, line)) {
        trim(line);
        if((!line.empty() and line.at(0) == 'c') or line.empty())
            continue;
        else if(line.size() >= 5 and line.substr(0,5) == "p cnf") {
            std::string sub = line.substr(6);
            trim(sub);
            std::vector<std::string> res;
            
            split(sub, res);
            
            if(res.size() != 2) {
                std::cout << res.size() << " - " << sub << std::endl;
                std::cerr << "Error: DIMACS CNF Header is invalid" << std::endl;
                return;
            }
            
            numVariables = std::stoi(res[0]);
            numClauses = std::stoi(res[1]);
            
            for(int i = 1; i <= numVariables; ++i) {
                variable v = {i, {}, {}, 1.0, 0};
                v.watchList.reserve(static_cast<size_t>(numClauses));
                variables.push_back(std::make_shared<variable>(v));
            }
        }
        else {
            std::vector<std::string> lineLits;
            split(line, lineLits);

            for(const auto &lit : lineLits) {
                if(lit.empty())
                    continue;
                int litInt = std::stoi(lit);
                
                literal parsedLit = {abs(litInt), litInt > 0};
                if(parsedLit.id > numVariables) {
                    std::cerr << "Error: Invalid literal: " << litInt << std::endl;
                    return;
                }
                if(parsedLit.id != 0)
                    literals.push_back(parsedLit);
                else {
                    if(literals.empty())
                        continue;
                    clause c{};
                    for(auto l : literals)
                        c.literals.push_back(std::make_shared<literal>(l));
                    
                    std::sort(c.literals.begin(), c.literals.end(), [](const auto &lhs, const auto &rhs){return lhs->id < rhs->id;});

                    auto sharedC = std::make_shared<clause>(c);

                    clauses.push_back(std::make_shared<clause>(c));
                    literals.clear();
                }
            }
        }
    }
    
    file.close();
    
    if(static_cast<int>(clauses.size()) != numClauses)
        std::cerr << "Error: Number of given clauses does not match header!" << std::endl;
    
    for(const auto &c : clauses){
        auto w1 = c->literals.front();
        auto w2 = c->literals.back();
        
        variableFor(*w1)->watchList.push_back(std::make_tuple(c, false));
        
        if(w1 != w2)
            variableFor(*w2)->watchList.push_back(std::make_tuple(c, false));
        
        c->watched1 = w1;
        c->watched2 = w2;
    }
    
    
}
