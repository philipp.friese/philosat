//
//  Solver.hpp
//  PhiloSAT
//
//  Created by Philipp Friese on 25.11.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#ifndef Solver_hpp
#define Solver_hpp

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <vector>
#include <memory>
#include <algorithm>
#include <atomic>

#include "Formula.hpp"

typedef enum {
    naive = 0,
    dlis = 1,
    dlcs = 2,
    mom = 3,
    jw = 4
} pickType;

typedef enum {
    forced = 0,
    branching = 1
} assignmentType;

typedef struct {
    assignmentType type;
    literal l;
    vValue oldValue;
    vValue newValue;
}assignment;

class Solver {
public:
    explicit Solver(Formula _formula) : formula(std::make_shared<Formula>(_formula)) {};
    
    std::vector<assignment> solve();
    
    std::atomic<bool> m_timeout = false;
    pickType ptype = pickType::mom;
    
    std::shared_ptr<Formula> formula;
private:
    literal pick();
    
    literal dlis();
    literal dlcs();
    literal mom();
    literal jw();
    literal naive();
    
    int set(literal l, vValue value, assignmentType type);
    void unset(literal l);
    
    int backtrack();
    
    int unitPropagation();
    int pureLiteralElimination();
    
    std::vector<assignment> alpha;
    std::vector<literal> units;
    std::vector<literal> pures;
};

#endif /* Solver_hpp */
