//
//  main.cpp
//  PhiloSAT
//
//  Created by Philipp Friese on 18.11.18.
//  Copyright © 2018 Philipp Friese. All rights reserved.
//

#include "Solver.hpp"
#include "Formula.hpp"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

std::vector<int> solveFormula(Solver &s)
{
    auto result = s.solve();
    if(result.empty())
        return {};
    
    std::vector<literal> satAssignment;
    
    while (!result.empty()) {
        assignment alpha = result.back();
        satAssignment.push_back({alpha.l.id, alpha.newValue == vTrue});
        result.pop_back();
    }
    
    std::stringstream ss;
    std::vector<int> solution;
    solution.reserve(s.formula->variables.size());
    for(auto &element : satAssignment)
        solution.push_back((element.sign ? 1 : -1) * element.id);
    
    for(const auto &element : s.formula->variables) {
        if(std::find_if(solution.begin(), solution.end(), [&element](const auto& var) { return abs(var) == element->id; }) == solution.end())
            solution.push_back(element->id);
    }
    
    std::sort(solution.begin(), solution.end(), [](const auto &lhs, const auto &rhs) { return abs(lhs) < abs(rhs); });
    return solution;
}

std::string getSatisfyingString(std::vector<int> result)
{
    std::stringstream ss;
    ss << "s ";
    ss << (result.empty() ? "UNSATISFIABLE" : "SATISFIABLE") << std::endl;
    
    if(!result.empty()) {
        ss << "v ";
        for(auto l : result)
            ss << l << " ";
        ss << "0" << std::endl;
    }
    return ss.str();
}


void printHelp() {
    std::cout << "PhiloSAT DPLL Solver, Version 1.0 by Philipp Friese" << std::endl;
    std::cout << "Usage: philosat [ <option> ] -f <input>" << std::endl;
    std::cout << " where <input> is the path to a DIMACS-conform cnf file" << std::endl;
    std::cout << " where <option> is one of the following: " << std::endl;
    std::cout << std::endl;
    std::cout << " -h         print this help message" << std::endl;
    std::cout << " -b [0-4]   set branching heuristic type (default 3 = MOM)" << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << " Branching heuristic types are mapped as follows: " << std::endl;
    std::cout << "  0 := Naive " << std::endl;
    std::cout << "  1 := DLIS (Dynamic Largest Individual Sum)" << std::endl;
    std::cout << "  2 := DLCS (Dynamic Largest Combined Sum)" << std::endl;
    std::cout << "  3 := MOM (Maximum Occurrences in clauses of Minimum width)" << std::endl;
    std::cout << "  4 := Jeroslow-Wang" << std::endl;
    
}
int main(int argc, char **argv)
{
    int opt;
    pickType type = pickType::mom;
    std::string filename = "";
    
    while ((opt = getopt(argc,argv,"hb:f:")) != EOF) {
        switch(opt) {
            case 'h':
                printHelp();
                break;
            case 'b': {
                long int parsedType = strtol(optarg, nullptr, 10);
                switch (parsedType) {
                    case 0:
                        type = pickType::naive;
                        break;
                    case 1:
                        type = pickType::dlis;
                        break;
                    case 2:
                        type = pickType::dlcs;
                        break;
                    case 3:
                        type = pickType::mom;
                        break;
                    case 4:
                        type = pickType::jw;
                        break;
                    default:
                        std::cerr << "Unknown Branching Heuristic type provided! Will use MOM heuristic." << std::endl;
                        break;
                }
                break;
            }
            case 'f':
                filename = std::string(optarg);
                break;
            default:
                break;
        }
    }
    
    if(filename.empty()) {
        printHelp();
        return -1;
    }
    
    Formula f(filename);
    Solver s(f);
    s.ptype = type;
    
    auto result = solveFormula(s);
    std::string satisfyingAssignment = getSatisfyingString(result);
    std::cout << satisfyingAssignment;
    
    return 0;
}
