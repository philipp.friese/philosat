# PhiloSAT

## Overview

This repository contains both a DPLL [1] and CDCL [2] SAT Solver, written in c++17 for a University project.

The project can be compiled via cmake and does not require any dependencies besides cmake (or a manual makefile).

Note: The result is purely academical and helped be understand how SAT solvers work. This solution is neither performant nor good. It may however provide a somewhat readable implementation of a DPLL and CDCL solver.

## Branch structure

The `master` branch contains the CDCL solver, the `DPLL` branch the DPLL solver.
The `CDCL` branch exists for legacy reasons.


## Techniques

The solver utilises the standard *CDCL* mechanism to derive a learned clause from a conflict. 

*Watched Literals* are utilised for fast unit propagation.

*VSIDS* is used as a branching heuristic.

*Restarts* are implemented using a *fixed*, *geometric* and *luby* technique.

*Preprocessing* is done in two was:

1. Deletion of tautological clauses such as ($`x, \bar{x}, y_i`$)
2. *Pure Literal Elimination*
3. *NiVER* for further clause deletion



## Benchmark

With one minute of computation time for each instance, the *fixed* and *geometric* restart techniques were able to solve 177 instances while the *luby* technique was able to solve 165 instances.

The different restart techniques are benchmarked below:

![Complete Benchmark](benchmark.png)

## Links

[1] https://doi.org/10.1145/368273.368557, https://doi.org/10.1145/321033.321034

[2] https://doi.org/10.1109/ICCAD.1996.569607, https://dl.acm.org/doi/10.5555/1867406.1867438
